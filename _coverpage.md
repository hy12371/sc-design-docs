![logo](_images/logo.png ':size=256x256')

# 慧耀云服务中台规划v1

> 服务中台（SC）现今划分：共享服务中心（SSC）、业务服务中台（BSC）

* 共享服务中心：已成熟的中台产品，对外提供规范的功能服务
* 业务服务中台：孵化中的中台产品，和业务息息相关随时会迭代变更
* 业务中台规划图、技术架构规划图、服务拆分说明、数据库设计说明

[Get Started](中台规划/什么是中台.md)