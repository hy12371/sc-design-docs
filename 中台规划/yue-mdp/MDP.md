Microservices Development Platform
微服务开发平台

## 功能特点
- 提供全套的微服务 + SOA + DevOps 架构体系
- 提供多种认证授权模式
- 提供主数据结构及同步规则
- 提供安全审计：用户审计日志、主数据审计日志、主数据版本审计

## 数据字典
<!-- > 附件资源：[sc_auth.pdf](../_media/sc_auth.pdf ':ignore')<br> -->
> 附件资源：[sc_auth.pdf](https://gitee.com/hy12371/sc-design-docs/raw/master/_media/sc_auth.pdf ':ignore')<br>
> 附件资源：[sc_master_data.pdf](https://gitee.com/hy12371/sc-design-docs/raw/master/_media/sc_master_data.pdf ':ignore')

### 主数据-表关系图解
![主数据-表关系](http://assets.processon.com/chart_image/5e01d06de4b0c1ff210825d9.png?_=1578389029259)
[<center>点击查看高清主数据-表关系图</center>](https://www.processon.com/view/link/5e01d06ee4b06f72408cb4b9)

### 主数据-多账户登陆设计思路
[👉多账户登陆设计](https://juejin.im/post/5e2ecec75188254c257c485d)

## 服务安全流程
![服务安全流程](MDP_files/1.png)

## 认证授权
认证授权流程图
![认证授权流程图](MDP_files/2.png)

OAuth2的角色和流程
![OAuth2的角色和流程](MDP_files/3.png)

基于spring cloud gateway的认证授权
![基于spring cloud gateway的认证授权](MDP_files/4.png)

## 授权模型
### 授权模型图解
![授权模型](MDP_files/1.jpg)

### 授权模型-表关系图解
![授权模型-表关系](http://assets.processon.com/chart_image/5e146399e4b0bcfb733817b7.png?_=1578397004868)
[<center>点击查看高清授权模型-表关系图</center>](https://www.processon.com/view/link/5e146399e4b0698ded31795f)

## 审计
